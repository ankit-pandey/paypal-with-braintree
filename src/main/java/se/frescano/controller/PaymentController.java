package se.frescano.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.frescano.services.BrainTreeService;

@RestController
public class PaymentController {
	
	@Autowired
	private BrainTreeService brainTreeService;
	
	@RequestMapping(path = "/createtransaction",method = {RequestMethod.POST},consumes = {MediaType.APPLICATION_JSON_VALUE})
	public String createTransaction(@RequestBody String body) throws IOException {
		
		System.out.println("nouce "+body);
		brainTreeService.createTransaction(body);
		return "success";
	}

}
