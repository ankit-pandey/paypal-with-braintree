package se.frescano.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.frescano.services.BrainTreeService;

@RestController
public class WelComeController {

	@Autowired
	private BrainTreeService brainTreeService;
    
	@CrossOrigin
	@RequestMapping(path = "/getclienttoken", method = {RequestMethod.GET}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Map<String, String> getclienttoken() {
		Map<String, String> model = new HashMap<>();
		model.put("clienttoken", brainTreeService.getClientToken());

		return model;
	}
	
	@CrossOrigin
	@RequestMapping(path = "/welcome", method = {RequestMethod.GET}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Map<String, String> welcome() {
		Map<String, String> model = new HashMap<>();
		model.put("message", "Hello");

		return model;
	}
	

}
