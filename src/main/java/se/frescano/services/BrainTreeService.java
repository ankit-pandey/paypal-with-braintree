package se.frescano.services;

import java.io.IOException;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.ClientTokenRequest;
import com.braintreegateway.Customer;
import com.braintreegateway.CustomerRequest;
import com.braintreegateway.Environment;
import com.braintreegateway.PaymentMethod;
import com.braintreegateway.PaymentMethodRequest;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class BrainTreeService {

	@Autowired
	private BraintreeGateway braintreeGateway;

	public String getClientToken() {
		// ClientTokenRequest clientTokenRequest = new
		// ClientTokenRequest().customerId("");

		return braintreeGateway.clientToken().generate();
	}

	public String createTransaction(String nonce) throws IOException {

		/*
		 * TransactionRequest request = new TransactionRequest() .amount(new
		 * BigDecimal("0.01")) .paymentMethodNonce(nouce)
		 * .orderId("Mapped to PayPal Invoice Number") .options()
		 * .submitForSettlement(true) .paypal() .customField("PayPal custom field")
		 * .description("Description for PayPal email receipt") .done()
		 * .storeInVaultOnSuccess(true) .done();
		 * 
		 * Result<Transaction> result = braintreeGateway.transaction().sale(request);
		 * result.getTransaction().getPayPalDetails().getToken(); if
		 * (result.isSuccess()) { Transaction transaction = result.getTarget();
		 * System.out.println("Success ID: " + transaction.getId()); } else {
		 * System.out.println("Message: " + result.getMessage()); }
		 */

		/*
		 * PaymentMethodRequest request = new PaymentMethodRequest()
		 * .customerId("131866") .paymentMethodNonce(nouce);
		 * 
		 * Result<? extends PaymentMethod> result =
		 * braintreeGateway.paymentMethod().create(request); String token =
		 * result.getTarget().getToken();
		 */
		ObjectMapper om = new ObjectMapper();
		String n = om.readTree(nonce).get("nouce").asText();
		System.out.println("Nounce " + n);
		CustomerRequest request = new CustomerRequest().id("2312312").paymentMethodNonce(n);

		Result<Customer> result = braintreeGateway.customer().create(request);

		// result.isSuccess();
		// true

		Customer customer = result.getTarget();
		System.out.println("Customer id " + customer.getId());
		// e.g. 160923

		String token = customer.getPaymentMethods().get(0).getToken();
		System.out.println("Token " + token);

		return "success";
	}

	public boolean makePayment(String customerId, String amount) {

		TransactionRequest transactionRequest = new TransactionRequest().amount(new BigDecimal(amount))
				.customerId(customerId).options().submitForSettlement(true).done();

		Result<Transaction> result = braintreeGateway.transaction().sale(transactionRequest);
		System.out.println("Transaction "+result.getTarget().getId());
		
		
		return result.isSuccess();
	}
	
	
	public boolean makeRefund(String transactionId) {
		
		Result<Transaction> result = braintreeGateway.transaction().refund(transactionId);
		System.out.println("Success : "+result.isSuccess());
		return result.isSuccess();
	}

}
