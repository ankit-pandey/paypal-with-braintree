package se.frescano;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;

import se.frescano.services.BrainTreeService;

@Configuration
public class BrainTreeConfiguration {
	
	@Value("${merchant}")
	private  String merchantId;
	@Value("${publickey}")
	private  String clientId;
	@Value("${privatekey}")
	private String secretId;
	
	@Bean
	public BrainTreeService getBrainTreeService() {
		return new BrainTreeService();
	}
	
	@Bean
	public BraintreeGateway  getBrainTreeGateWay() {
		
		return new BraintreeGateway(
				  Environment.SANDBOX,
				  merchantId,
				  clientId,
				  secretId
				);
	}

}
