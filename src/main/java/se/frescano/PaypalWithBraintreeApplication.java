package se.frescano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaypalWithBraintreeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaypalWithBraintreeApplication.class, args);
	}

}
